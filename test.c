#include"matrix.h"
#include<stdio.h>

int main()
{
  int i, j, r, c;
  double data;
  scanf("%d%d", &r, &c);
  Matrix A = alloc(r,c);
  for(i=0; i<r; i++)
      for(j=0; j<c; j++)
	scanf("%lf", &A.M[i][j]);
  display(A);
  return 0;
}
