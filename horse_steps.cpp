/*
 * Minimum steps required for a horse in chess
 * to reach from a point (x1,y1) to (x2,y2)
 */
#include<stdio.h>
#include<iostream>

#define N 8
#define fr(i,s,e) for(int i=s; i<e; i++)

bool eq(int pt1[2], int pt2[2]) {
  if (pt1[0] == pt2[0] && \
      pt1[1] == pt2[1])
    return true;
  return false;
}

void horse_steps(int pt[2], int ctr) {
  // 2 1, 2 -1, -2 1, -2 -1, 1 2, 1 -2, -1 2, -1 -2.
  int X[9] = {2,2,-2,-2,1,1,-1,-1,0};
  int Y[9] = {1,-1,1,-1,2,-2,2,-2,0};
  if (ctr>8) return;
  if (ctr==0) {
    pt[0] += 2;
    pt[1] += 1;
  }
  else {
    pt[0] += (X[ctr] - X[ctr-1]);
    pt[1] += (Y[ctr] - Y[ctr-1]);
  }
  return;
}   

bool isvalid(int pt[2]) {
  if (pt[0]<0 || pt[1]<0 || \
      pt[0]>7 || pt[1]>7)
    return false;
  return true;
}

bool visited(int pt[2]) {
  static int visited[N][N];
  if (pt[0] == -1 && pt[1] == -1) {
    fr(i,0,N) fr(j,0,N) visited[i][j] = 0;
  }
  if (!isvalid(pt)) return false;
  if (visited[pt[0]][pt[1]]) return true;
  visited[pt[0]][pt[1]] = 1;
  return false;
}

int BFS(int pt_s[2], int pt_d[2]) {
  int stack[N*N];
  int s=0, e=0;
  int steps=0;
  int pt[2] = {pt_s[0], pt_d[1]};
  stack[e++] = 10*(pt_s[0]*8 + pt_s[1]) + steps;

  while(!eq(pt, pt_d)) {
    if (s>e) break;
    steps = stack[s]%10 + 1;
    stack[s] /= 10;
    pt[0] = stack[s]/8;
    pt[1] = stack[s]%8;
    s++;
    //printf("steps: %d\t pt: (%d,%d)\n", steps, pt[0], pt[1]);
    for(int i=0; i<8; i++){
      horse_steps(pt, i);
      //printf(" -> (%d,%d)\n", pt[0], pt[1]);
      if (!isvalid(pt)) continue;
      if (eq(pt, pt_d)) break;
      if (!visited(pt)) {
	stack[e++] = 10*(pt[0]*8 + pt[1]) + steps;
      }
    }
  }
  //printf("pt: (%d,%d)\tpt_d: (%d, %d)\n", pt[0], pt[1], pt_d[0], pt_d[1]);
  return steps;
}

    
int main() {
  int A[N][N];
  int t;
  scanf("%d", &t);
  int init[2] = {-1,-1};
  while(t-->0) {
    int p1[2], p2[2];
    scanf("%d%d%d%d", p1, p1+1, p2, p2+1);
    visited(init);
    printf("%d\n", BFS(p1, p2));
  }
  return 0;
}
