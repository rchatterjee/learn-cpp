#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node{
	//int value;
	char *word;
	struct node* right;
	struct node* left;
} Node;

int main()
{
    int len, cnt;
	FILE *fp;
    fp = fopen("gutenberg.txt","r");
	//scan the first word..
	char *string, *string1;
	Node *head, *p;
	len = strlen(string);
	fscanf(fp,"%[^ ]s",string);
	head = (Node*)malloc(len*sizeof(Node));
	head->word = string;
	head->right = head->left = NULL;
	while(feof(fp))
	{
		fscanf(fp,"%[^\n]s",string);
		p = head;
		for(;;)
		{
			if(p->word != NULL)
			{
				string1 = p->word;
				if(strcmp(string1,string)>0)
				{
					if(p->right!=NULL)
						p = p->right;
					else
					{
						len = strlen(string);
						p->right = (Node*)malloc(len*sizeof(Node));
						p = p->right;
					}
				}
				else if(strcmp(string1,string)<0)
				{
					if(p->left!=NULL)
						p = p->left;
					else
					{
						len = strlen(string);
						p->left = (Node*)malloc(len*sizeof(Node));
						p = p->left;
					}
				}
				else
					break;
			}
			else
			{
				p->word = string;
				p->right = p->left = NULL;
				cnt++;
				break;
			}
		}
	}
	printf("%d", cnt);
}

