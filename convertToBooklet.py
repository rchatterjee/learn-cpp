#/usr/bin/python

import PyPDF2 as ppdf2
import os
"""Converts a pdf file into booklet style pdf file, so that you can
just, print the file using normal printing command (such as 'lpr
<document.pdf>').
----------          ------------
|        |          | -------- |
|        |          ||        ||
|        |          ||________||
|        | --->     | ________ |
|        |          ||        ||
|        |          ||        ||
|________|          | -------- |
                     ----------

"""

def get_page_list(n):
    num_sheets = int(n/4.0 + 0.5)
    sheets = [[-1 for _ in xrange(4)] 
              for i in xrange(num_sheets)]
    cnt = 0
    for i in xrange(num_sheets):
        sheets[i][1] = cnt
        sheets[i][2] = cnt+1
        cnt += 2
    for i in xrange(num_sheets-1, -1,-1):
        if cnt<n:
            sheets[i][3] = cnt+1;
            cnt += 1
        else:
            break
        if cnt<n:
            sheets[i][0] = cnt+2;
            cnt += 1
        else:
            break
    return sheets

def convert_to_booklet(pdffile):
    ipdf = ppdf2.PdfFileReader(pdffile)
    opdf = ppdf2.PdfFileWriter()
    oldpagelist = range(ipdf.getNumPages())
    page_numbers = get_page_list(ipdf.getNumPages())
    print page_numbers
    for pn in page_numbers:
        page = ppdf2.pdf.PageObject(opdf)
        if pn[0] != -1:
            page.mergeRotatedScaledTranslatedPage(ipdf.getPage(pn[0]),
                                                  -90, 0.5, 0.0, 0.0, True)
        if pn[1] != -1:
            page.mergeRotatedScaledTranslatedPage(ipdf.getPage(pn[0]),
                                                  -90, 0.5, 0.0, 0.5, True)
        # opdf.addPage(page)
        page = ppdf2.pdf.PageObject(opdf)
        if pn[2] != -1:
            page.mergeRotatedScaledTranslatedPage(ipdf.getPage(pn[0]),
                                                  -90, 0.5, 0.0, 0.0, True)
        if pn[3] != -1:
            page.mergeRotatedScaledTranslatedPage(ipdf.getPage(pn[0]),
                                                  -90, 0.5, 0.0, 0.5, True)
        # opdf.addPage(page)
    opdf.write(open('127_test.pdf', 'wb'))

import sys
convert_to_booklet(sys.argv[1])
