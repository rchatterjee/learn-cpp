#include<stdio.h>
#include<stdlib.h>

#define newline printf("\n")


void display(int*, int, int);
void mergesort(int*, int, int);

int main()
{
  int len, arr[30], result[30], data;
  len = 0;
  printf("enter the numbers and press ctrl+d to terminate\n\n");
  while(scanf("%d",&data) > 0)
    {
      arr[len++] = data;
      result[len-1] = data;
    }
  
  display(result, 0, len-1);
  mergesort(result,len-1,0);
  display(result, 0, len-1);
}

void mergesort(int *arr, int up, int down)
{
  int j, k;
  static count =0;
  int mid = (up + down)/2;
  int left = down;
  int right= mid+1;
  // printf("l: %d, r: %d, m: %d\n", left, right, mid);
  if(up - down > 1)
    {
      mergesort(arr, mid, down);
      mergesort(arr, up, mid+1);
      while(right<=up && left<=right)
	{
	  if(arr[right] >= arr[left])
	    {
	      left++;
	    }
	  else if(arr[left] > arr[right] )
	    {
	      int t = arr[right];
	      for(k=right; k>left; k--)
		arr[k] = arr[k-1];
	      arr[left++] = t;
	      right++;
	    }
	}
    }
  else
    {
      if (arr[down]>arr[up]) {
	int t = arr[up];
	arr[up]=arr[down];
	arr[down] = t;
      }
    }
  //display(arr, down, up);
} 

void display(int* arr, int l, int r)
{
  int j;
  for(j=l; j<=r; j++)
    printf("%4d, ", arr[j]);
  newline;
}
