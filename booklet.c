#include<stdio.h>

int main(void)
{
  int i, n, cnt = 0;
  printf("enter the number of pages\n");
  scanf("%d", &n);
  int sheet = (int)(n/4.0+0.5);
  int arr[sheet][4];
  arr[0][2] = arr[1][2] = -1;
  arr[0][3] = arr[1][3] = -1;
  for(i=0;i<sheet;i++)
    {
      arr[i][0] = cnt++;
      arr[i][1] = cnt++;
    }
  for(i=sheet-1;i>=0;i--) 
    {
      if(cnt<n)
        arr[i][2] = cnt++;
      else
        break;
      if(cnt<n)
        arr[i][3] = cnt++;
      else
        break;
    }
  for(i=0;i<sheet;i++)
    {
      printf("Sheet %d will consist these pages sequentially -> %d, %d, %d, %d\t\n", i+1, arr[i][3], arr[i][0], arr[i][1], arr[i][2]);
    }
}
