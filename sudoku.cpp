#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int rowcheck(int arr[][9],int i);
int columncheck(int arr[][9], int i);
int squarecheck(int arr[][9], int r,int c);

int main()
{
  // Try use i as row index and j as column, avoid j1, j2 
  // if required give bigger name, but not i1, i2, j3 etc.
  // reuse the variable name
  static int arr[9][9];
  int i, n , r, c;
  printf("give the entries and press 0 as number to terminate\n");
  for(;;) {
    printf("enter the location in x,y (values 0,1,2..7,8)\n");
    scanf("%d%d",&r,&c);
    
    if((r<0)||(r>8)||(c<0)||(c>8)){
      printf("invalid entry ,,,,,RETRY\n");
      continue;
    }
  
    printf("enter the number\n");
    scanf("%d",&arr[r][c]);
    
    if(arr[r][c]>9||arr[r][c]<0)
      {
	printf("invalid entry\n");
	arr[r][c] = 0;
      }
    if(arr[r][c]==0)
      break;
  }
  // Checking validity of rows
  for(i=0; i<9; i++)
    rowcheck(arr,i);

  // Checking validity of columns 
  for(i=0;i<9;i++)
    columncheck(arr,i);
  
  // Checking validity of squares
  for(r=0;r<3;r++)
    for(c=0;c<3;c++)
      squarecheck(arr,r,c);
  return 0;
}

int rowcheck(int arr[][9],int i)
{
  int check[9];  
  int j;
  for(j=0;j<9;j++) check[j] = 0;
  for(j=0;j<9;j++)
    {
      if(arr[i][j]!=0)
	check[arr[i][j]-1]++;
    }
  
  for(j=0;j<9;j++)
    {
      if(check[j]>1) {
	printf("sudoku entry is invalid fault in %d %d th block",i,j);
	return 0;
      }
    }
  return 1;
}


int columncheck(int arr[][9], int j)
{
  int check[9];
  int i;
  for(i=0;i<9;i++) check[i] = 0;
  
  for(i=0;i<9;i++)
    {
      if(arr[i][j]!=0)
	check[arr[i][j]-1]++;
    }
  
  for(i=0;i<9;i++)
    {
      if(check[i]>1) {
	printf("sudoku entry is invalid fault in %d %d th block",i,j);
	return 0;
      }
    }
  return 1;
}

int squarecheck(int arr[][9], int r,int c)
{
  int check[9];
  int i, j;
  for(j=0;j<9;j++) check[j] = 0;
  for(i=3*r;i<3*r+3;i++) {
    for(j=3*c;j<3*c+3;j++)
      {
	if(arr[i][j]=!0)
	  check[arr[i][j]-1]++;
      }
  }
  
  for(i=0;i<9;i++)
    if(check[i]>1) {
      printf("sudoku entry is invalid");
      return 0;
    }
  
  return 1;
}
	  

