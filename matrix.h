#ifndef __MATRIX_H__
#define __MATRIX_H__
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long int uint64_t;

typedef struct matrix{
  double **M;
  uint16_t r, c;
} Matrix;

// To allocate and deallocate memory
Matrix alloc(int r, int c);
void delete(Matrix A);
void copy(Matrix A, Matrix B);

void mult_matrix(Matrix A, Matrix B);
void add_matrix(Matrix A, Matrix B);
void det(Matrix A);
void adjoint(Matrix A);
void inverse(Matrix A, Matrix B);

void mult_scalar(Matrix A, float f);
void add_scalar(Matrix A, float f);

void display(Matrix A);

#endif
