#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void check(int);
void print();

int a[4][6];

int main()
{
	int i, j, x, y, col, val, cnt = 0;
	char ch;
	for(i=0;i<4;i++)
		for(j=0;j<6;j++)
			a[i][j] = 0;
	while(cnt<20)
	{
		x = rand()%4;
		y = rand()%6;
		if(a[x][y] == 0)
		{
			a[x][y] = ((cnt/5) + 1)*10 + cnt%5;
			cnt++;
		}
	}
	val = 0;
	for(;;)
	{
		if(val>10)
		{
			check(val);
		}
		print();
        //	win();
		printf("enter block character\n");
		scanf("%c", &ch);
		printf("enter block number\n");
		scanf("%d", &val);
		fflush(stdin);
		val = val - 1;
		if(ch == '@')
			col = 1;
		else if(ch == '#')
			col = 2;
		else if(ch == '$')
			col = 3;
		else if(ch == '*')
			col = 4;
		else
		{
			printf("wrong input ....try again\n");
			col = 0;
		}
		val = col*10 + val;
	}
}

void check(int val)
{
	int i, j, x, y, flag = 0;
	for(i=0;i<4;i++)
		for(j=0;j<6;j++)
		{
			if(a[i][j] == val)
			{
				x = i;
				y = j;
				break;
			}
		}
	if(val%10 == 0)
	{
		for(i=0;i<4;i++)
		{
			if(a[i][0] == 0)
			{
				a[i][0] = val;
				a[x][y] = 0;
				flag = 1;
				break;
			}
		}
		if(flag == 0)
		{
			printf("no move possible..... TRY AGAIN\n");
			return;
		}
	}
	else
	{
		for(i=0;i<4;i++)
			for(j=0;j<6;j++)
			{
				if(a[i][j] == val-1)
				{
					if(j<5)
						j = j + 1;
					else
					{
						printf("no move possible..... TRY AGAIN\n");
						return;
					}
					goto label;
				}
			}
		label: if(a[i][j] != 0)
		{
			printf("no move possible..... TRY AGAIN\n");
			return;
		}
		else
		{
			a[i][j] = val;
			a[x][y] = 0;
		}	
	}
}

void print()
{
	int i, j, k;
	for(i=0;i<4;i++)
	{
		for(j=0;j<6;j++)
		{
			if(a[i][j]/10 == 1)
			{
				printf("@%d ", a[i][j]%10+1);
				printf("||");
			}
			else if(a[i][j]/10 == 2)
			{
				printf("#%d ", a[i][j]%10+1);
				printf("||");
			}
			else if(a[i][j]/10 == 3)
			{
				printf("$%d ", a[i][j]%10+1);
				printf("||");	
			}
			else if(a[i][j]/10 == 4)
			{
				printf("*%d ", a[i][j]%10+1);
				printf("||");
			}
			else
				printf("   ||");
		}
		printf("\n");
		for(k=0;k<30;k++)
		{
			printf("_");
		}
		printf("\n");
	}
}






