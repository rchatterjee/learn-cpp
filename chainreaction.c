#include<stdio.h>
#include<math.h>
#include<stdlib.h>

#define fr(i,p,q) for(i=p;i<q;i++)

void win(int);
void check(int);
void blast(int,int,int);
void input(int,int,int);
void print();

int a[10][10];

int main()
{
  int i, j, x, y, count, n, cnt, win_check = 0;
  count = 0;
  fr(i,0,10)
    fr(j,0,10)
    {
      a[i][j] = 0;
    }
  printf("enter number of players(2-6)\n");
  scanf("%d", &n);
  if(n >= 2)
    printf("player 1 --> *\t player 2 --> #");
  if(n >= 3)
    printf("\t player 3 --> @");
  if(n >= 4)
    printf("\t player 4 --> $");            
  if(n >= 5)
    printf("\t player 5 --> &");
  if(n >= 6)
    printf("\t player 6 --> ?");
  printf("\n");

  for(;;)
    {
      cnt = count%n + 1;
      printf("enter co rodinate for player %d\n", cnt);
      scanf("%d%d", &x, &y);
      if(a[x][y]==0||a[x][y]/10==cnt)
        {
	  input(x,y,cnt);
	  count++;
	  win_check++;
	  print();
	  if(win_check>n)
	    win(cnt);
        }
      else
	printf("invalid input\n   TRY AGAIN\n");
    }
}

void input(int x, int y, int cnt)
{
  if(a[x][y]==0)
    a[x][y] = cnt*10 + 1;
  else
    a[x][y]++;
  check(cnt);
}

void check(int cnt)
{
  int i, j, chk = 0;
  fr(i,0,10)
    fr(j,0,10)
    {
      if(i==0||i==9||j==0||j==9)
	{
	  if(i+j==9||i==j)
	    {
	      if(a[i][j]%10>1)
		{
		  blast(i,j,cnt);
		  a[i][j] = a[i][j] - 2;
		  chk++;
		}
	    }
	  else
	    {
	      if(a[i][j]%10>2)
		{
		  blast(i,j,cnt);
		  a[i][j] = a[i][j] - 3;
		  chk++;
		}
	    }
	}
      else
	{
	  if(a[i][j]%10>3)
	    {
	      blast(i,j,cnt);
	      a[i][j] = a[i][j] - 4;
	      chk++;
	    }
	}
      if(a[i][j]%10 == 0)
	a[i][j] = 0;
    }
  if(chk>0)
    check(cnt);
}

void blast(int i, int j, int cnt)
{
  printf("calling blast in %d  %d\n", i, j);
  a[i+1][j] = a[i+1][j]%10 + cnt*10 + 1;
  a[i][j+1] = a[i][j+1]%10 + cnt*10 + 1;
  a[i][j-1] = a[i][j-1]%10 + cnt*10 + 1;
  a[i-1][j] = a[i-1][j]%10 + cnt*10 + 1;
}

void win(int cnt)
{
  int m, n, flag = 0;
  fr(m,0,10)
    fr(n,0,10)
    {
      if(a[m][n]/10 != 0 && a[m][n]/10 != cnt)
	flag = 1;
    }
  if(flag == 0)
    {
      printf("player %d wins the game\n", cnt);
      exit(0);
    }
}

void print()
{
  int i, j, ch, no;
  printf(" \t");
  fr(j,0,10)
    printf(" %d   ", j);
  printf("\n\n");
  fr(i,0,10)
    {
      printf("%d\t", i);
      fr(j,0,10)
	{
	  ch = a[i][j]/10;
	  if(ch>0)
	    {
	      if(ch == 1)
		printf("%d* ", a[i][j]%10);
	      else if(ch == 2)
		printf("%d# ", a[i][j]%10);
	      else if(ch == 3)
		printf("%d@ ", a[i][j]%10);
	      else if(ch == 4)
		printf("%d$ ", a[i][j]%10);
	      else if(ch == 5)
		printf("%d& ", a[i][j]%10);
	      else if(ch == 6)
		printf("%d? ", a[i][j]%10);
	    }                    
	  else
	    printf("   ");
	  printf("||");    
	}
      printf("\n\t");
      fr(no,0,10)
	printf("---  ");    
      printf("\n");
    }
}
