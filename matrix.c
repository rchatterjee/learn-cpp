#include<stdio.h>
#include<stdlib.h>
#include"matrix.h"

void display(Matrix A) 
{
  int i, j;
  for(i=0;i<A.r;i++)
    {
      for(j=0;j<A.c;j++)
	printf("%6.2lf ", A.M[i][j]);
      printf("\n");
    }
}

Matrix alloc(int r, int c)
{
  int i;
  Matrix R;
  R.r = r;
  R.c = c;
  R.M = (double**)malloc(sizeof(double*)*r);
  for(i=0;i<r;i++)
    R.M[i] = (double*)malloc(sizeof(double)*c);
  return R;
}

